import requests
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import queue
import threading
from w3lib import url
import networkx as nx
import matplotlib.pyplot as plt

NUM_THREADS = 10

class Page:
    """
    Encapsulates a page represented by it's canonicalized url and all it's linked pages.
    """
    def __init__(self, clink):
        self.url = clink
        self.links = set()

    def __eq__(self, other):
        return self.url == other.url

    def __hash__(self):
        return hash(self.url)

class PageFactory:
    """
    Thread safe version of a cache for looking up already fetched pages.
    """
    pagemap = dict()
    lock = threading.Lock()

    def get(self, link):
        """
        Look up the cache for link and return cached object.

        :param link: url to look up
        :return: tuple of page, visited where visited = True means
                 it was served out of cache. visisted = False means
                 new object was created.
        """
        clink = url.canonicalize_url(link)
        visited = True
        self.lock.acquire()
        if clink not in self.pagemap:
            print(clink)
            visited = False
            self.pagemap[clink] = Page(clink)

        self.lock.release()
        return self.pagemap[clink], visited

class Scraper:
    """
    Multi-threaded site scraper.
    """
    site = ''
    workq = queue.Queue()
    page_factory = PageFactory()
    threads = []

    def __init__(self, site):
        self.site = site

    def filter_link(self, link, parent):
        """
        Fixup and filter links found in page.

        - For absolute links check if it belongs to toplevel domain.
        - For relative links, convert to fully qualified url
        - For Nav links, ignore and return ''

        :param link: link
        :param parent: url of page link was found in.
        :return: Fixed up link or empty if link was not relevant.
        """
        if link.startswith('http'):
            link_domain = urlparse(link).netloc
            parent_domain = urlparse(self.site).netloc
            if link_domain.endswith(parent_domain):
                return link
            else:
                return ''
        elif link.startswith('/'):
            parts = urlparse(parent.url)
            return parts.scheme + '://' + parts.netloc + link
        else:
            # Nav-link, ignore
            return ''
        
    def scrape(self, maxdepth):
        """
        Initialize job queue, start worker threads and block till completion.
        :param maxdepth: No. of levels of links to scan.
        """
        page, _ = self.page_factory.get(self.site)
        self.workq.put((page, maxdepth))

        for i in range(NUM_THREADS):
            t = threading.Thread(target=self.worker)
            t.start()
            self.threads.append(t)

        self.workq.join()
        for i in range(NUM_THREADS):
            self.workq.put(None)
        for t in self.threads:
            t.join()
        print("")

    def worker(self):
        """
        Thread pool workerfn
        """
        while self.process_page():
            pass

    def get_internal_links(self, page):
        """
        Downloads the page, scans for links and returns list of internal links.
        :param page: page to scan for links
        :return: list of internal links
        """
        response = requests.get(page.url, timeout=5)
        content = BeautifulSoup(response.content, "html.parser")
        links = [self.filter_link(l.get('href'), page) for l in content.findAll('a', href=True)]
        return [l for l in links if l]
        
    def process_page(self):
        """
        Process single item from queue.
        :return: Return true if item was valid
        """
        item = self.workq.get()
        if not item:
            return False

        page, depth = item
        try:
            internal_links = self.get_internal_links(page)
        except:
            print('ERROR: Could not download URL', page.url)
            self.workq.task_done()
            return True
        for link in internal_links:
            child_page, visited = self.page_factory.get(link)
            if not visited and depth > 0:
                self.workq.put((child_page, depth - 1))
            page.links.add(child_page)

        self.workq.task_done()
        return True

    def render_site_map(self):
        """
        Convert to networkx graph and render using matplotlib.
        """
        G = nx.Graph()
        for page in self.page_factory.pagemap.values():
            G.add_node(page)
            for link in page.links:
                G.add_edge(page, link)
        nx.draw_shell(G, with_labels=False)
        plt.show()

if __name__ == "__main__":
    scraper = Scraper('https://monzo.com')
    scraper.scrape(maxdepth=5)
    scraper.render_site_map()
    
    
